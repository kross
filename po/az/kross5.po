# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kross package.
#
# Xəyyam <xxmn77@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kross\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-10-24 02:12+0200\n"
"PO-Revision-Date: 2021-01-01 18:24+0400\n"
"Last-Translator: Kheyyam Gojayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Xəyyam"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xxmn77@gmail.com"

#: console/main.cpp:99
#, kde-format
msgctxt "application name"
msgid "Kross"
msgstr "Kross"

#: console/main.cpp:101
#, kde-format
msgctxt "application description"
msgid "Command-line utility to run Kross scripts."
msgstr "Kross skriptlərini başlatmaq üçün proqram."

#: console/main.cpp:103
#, kde-format
msgctxt "@info:credit"
msgid "Copyright 2006 Sebastian Sauer"
msgstr "Copyright 2006 Sebastian Sauer"

#: console/main.cpp:107
#, kde-format
msgctxt "@info:credit"
msgid "Sebastian Sauer"
msgstr "Sebastian Sauer"

#: console/main.cpp:108
#, kde-format
msgctxt "@info:credit"
msgid "Author"
msgstr "Müəllif"

#: console/main.cpp:117
#, kde-format
msgctxt "@info:shell command-line argument"
msgid "The script to run."
msgstr "Başlatmaq üçün skript."

#: core/action.cpp:481
#, kde-format
msgid "Scriptfile \"%1\" does not exist."
msgstr "\"%1\" skript faylı tapılmadı."

# İnterpretator - Kodun bütün təlimatlarını yerinə yetirib, ardıcıl onu açan proqram.
#: core/action.cpp:485
#, kde-format
msgid "Failed to determine interpreter for scriptfile \"%1\""
msgstr "\"%1\" skripti üçün interpretatoru təyin etmək mümkün olmadı"

#: core/action.cpp:489
#, kde-format
msgid "Failed to open scriptfile \"%1\""
msgstr "\"%1\" skript faylını açmaq mümkün olmadı"

#: core/action.cpp:500
#, kde-format
msgid "Failed to load interpreter \"%1\""
msgstr "\"%1\" interpretatorunu yükləmək mümkün olmadı"

#: core/action.cpp:502
#, kde-format
msgid "No such interpreter \"%1\""
msgstr "Naməlum \"%1\" interpretatoru"

#: core/action.cpp:509
#, kde-format
msgid "Failed to create script for interpreter \"%1\""
msgstr "\"%1\" interpretatoru üçün skript yaradıla bilmədi"

#: core/manager.cpp:152
#, kde-format
msgid "Level of safety of the Ruby interpreter"
msgstr "Ruby interpretatorunun təhlükəsizlik səviyyəsi"

#: modules/form.cpp:327
#, kde-format
msgid "Cancel?"
msgstr "İmtina etmək?"

#: qts/values_p.h:76
#, kde-format
msgid "No such function \"%1\""
msgstr "\"%1\" funksiyası mövcud deyil"

#: ui/actioncollectionview.cpp:174
#, kde-format
msgid "Name:"
msgstr "Ad:"

#: ui/actioncollectionview.cpp:182
#, kde-format
msgid "Text:"
msgstr "Mətn:"

#: ui/actioncollectionview.cpp:189
#, kde-format
msgid "Comment:"
msgstr "Şərh:"

#: ui/actioncollectionview.cpp:196
#, kde-format
msgid "Icon:"
msgstr "İkon:"

#: ui/actioncollectionview.cpp:217
#, kde-format
msgid "Interpreter:"
msgstr "İnterpretator:"

#: ui/actioncollectionview.cpp:233
#, kde-format
msgid "File:"
msgstr "Fayl:"

#: ui/actioncollectionview.cpp:326
#, kde-format
msgid "Run"
msgstr "Başlat"

#: ui/actioncollectionview.cpp:328
#, kde-format
msgid "Execute the selected script."
msgstr "Seçilmiş skripti icra et."

#: ui/actioncollectionview.cpp:333
#, kde-format
msgid "Stop"
msgstr "Dayandırmaq"

#: ui/actioncollectionview.cpp:335
#, kde-format
msgid "Stop execution of the selected script."
msgstr "Seşilmiş skriptin icrasını dayandır."

#: ui/actioncollectionview.cpp:340
#, kde-format
msgid "Edit..."
msgstr "Düzəliş..."

#: ui/actioncollectionview.cpp:342
#, kde-format
msgid "Edit selected script."
msgstr "Seçilmiş skriptə düzəliş et."

#: ui/actioncollectionview.cpp:347
#, kde-format
msgid "Add..."
msgstr "Əlavə et..."

#: ui/actioncollectionview.cpp:349
#, kde-format
msgid "Add a new script."
msgstr "Yeni skript əlavə et."

#: ui/actioncollectionview.cpp:354
#, kde-format
msgid "Remove"
msgstr "Sil"

#: ui/actioncollectionview.cpp:356
#, kde-format
msgid "Remove selected script."
msgstr "Seçilmiş skripti sil."

#: ui/actioncollectionview.cpp:549
#, kde-format
msgid "Edit"
msgstr "Düzəliş"

#: ui/actioncollectionview.cpp:554
#, kde-format
msgctxt "@title:group Script properties"
msgid "General"
msgstr "Əsas"
