# Translation of kross5 to Norwegian Nynorsk
#
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 2003, 2004, 2005, 2006.
# Håvard Korsvoll <korsvoll@skulelinux.no>, 2003, 2005.
# Karl Ove Hufthammer <karl@huftis.org>, 2004, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kdelibs4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-10-24 02:12+0200\n"
"PO-Revision-Date: 2014-10-05 09:59+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Gaute Hvoslef Kvalnes,Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "gaute@verdsveven.com,karl@huftis.org"

#: console/main.cpp:99
#, kde-format
msgctxt "application name"
msgid "Kross"
msgstr "Kross"

#: console/main.cpp:101
#, kde-format
msgctxt "application description"
msgid "Command-line utility to run Kross scripts."
msgstr "Kommandolinjeprogram for køyring av Kross-skript."

#: console/main.cpp:103
#, kde-format
msgctxt "@info:credit"
msgid "Copyright 2006 Sebastian Sauer"
msgstr "© 2006 Sebastian Sauer"

#: console/main.cpp:107
#, kde-format
msgctxt "@info:credit"
msgid "Sebastian Sauer"
msgstr "Sebastian Sauer"

#: console/main.cpp:108
#, kde-format
msgctxt "@info:credit"
msgid "Author"
msgstr "Opphavsperson"

#: console/main.cpp:117
#, kde-format
msgctxt "@info:shell command-line argument"
msgid "The script to run."
msgstr "Skriptet som skal køyrast."

#: core/action.cpp:481
#, kde-format
msgid "Scriptfile \"%1\" does not exist."
msgstr "Skriptfila «%1» finst ikkje"

#: core/action.cpp:485
#, kde-format
msgid "Failed to determine interpreter for scriptfile \"%1\""
msgstr "Klarte ikkje finna tolk til skriptfila «%1»."

#: core/action.cpp:489
#, kde-format
msgid "Failed to open scriptfile \"%1\""
msgstr "Klarte ikkje opna skriptfila «%1»."

#: core/action.cpp:500
#, kde-format
msgid "Failed to load interpreter \"%1\""
msgstr "Klarte ikkje lasta inn tolken «%1»"

#: core/action.cpp:502
#, kde-format
msgid "No such interpreter \"%1\""
msgstr "Tolken «%1» finst ikkje"

#: core/action.cpp:509
#, kde-format
msgid "Failed to create script for interpreter \"%1\""
msgstr "Klarte ikkje laga skript til tolken «%1»"

#: core/manager.cpp:152
#, kde-format
msgid "Level of safety of the Ruby interpreter"
msgstr "Tryggleiksnivå på Ruby-tolkaren"

#: modules/form.cpp:327
#, kde-format
msgid "Cancel?"
msgstr "Avbryt?"

#: qts/values_p.h:76
#, kde-format
msgid "No such function \"%1\""
msgstr "Funksjonen «%1» finst ikkje"

#: ui/actioncollectionview.cpp:174
#, kde-format
msgid "Name:"
msgstr "Namn:"

#: ui/actioncollectionview.cpp:182
#, kde-format
msgid "Text:"
msgstr "Tekst:"

#: ui/actioncollectionview.cpp:189
#, kde-format
msgid "Comment:"
msgstr "Merknad:"

#: ui/actioncollectionview.cpp:196
#, kde-format
msgid "Icon:"
msgstr "Ikon:"

#: ui/actioncollectionview.cpp:217
#, kde-format
msgid "Interpreter:"
msgstr "Fortolkar:"

#: ui/actioncollectionview.cpp:233
#, kde-format
msgid "File:"
msgstr "Fil:"

#: ui/actioncollectionview.cpp:326
#, kde-format
msgid "Run"
msgstr "Køyr"

#: ui/actioncollectionview.cpp:328
#, kde-format
msgid "Execute the selected script."
msgstr "Køyr det valde skriptet."

#: ui/actioncollectionview.cpp:333
#, kde-format
msgid "Stop"
msgstr "Stopp"

#: ui/actioncollectionview.cpp:335
#, kde-format
msgid "Stop execution of the selected script."
msgstr "Stopp køyringa av det valde skriptet."

#: ui/actioncollectionview.cpp:340
#, kde-format
msgid "Edit..."
msgstr "Rediger …"

#: ui/actioncollectionview.cpp:342
#, kde-format
msgid "Edit selected script."
msgstr "Rediger vald skript."

#: ui/actioncollectionview.cpp:347
#, kde-format
msgid "Add..."
msgstr "Legg til …"

#: ui/actioncollectionview.cpp:349
#, kde-format
msgid "Add a new script."
msgstr "Legg til nytt skript."

#: ui/actioncollectionview.cpp:354
#, kde-format
msgid "Remove"
msgstr "Fjern"

#: ui/actioncollectionview.cpp:356
#, kde-format
msgid "Remove selected script."
msgstr "Fjern valt skript."

#: ui/actioncollectionview.cpp:549
#, kde-format
msgid "Edit"
msgstr "Rediger"

#: ui/actioncollectionview.cpp:554
#, kde-format
msgctxt "@title:group Script properties"
msgid "General"
msgstr "Generelt"
